# Programa que lea repetidamente números hasta que el
# usuario introduzca "fin". Una vez que se haya introducido
# "fin", muestra por pantalla el total, la cantidad de
# números y la media de esos números. Si el usuario introduce
# cualquier otra cosa que no sea un número, detecta su fallo
# usando try y except, muestra un mensaje de error y pasa al
# nùmero siguiente.

# ___Author___ = "Danny Lima"
# ___Email___ = "danny.lima@unl.edu.ec

contador=0
suma=0

while True:
    numero=input("Ingrese un número: ")
    if numero.lower() in "fin":
        break
    try:
        suma = suma + int(numero)
        contador = contador + 1
        promedio = suma / contador

    except ValueError:
        print("Entrada inválida")


print("La suma es: ", suma)
print("La total de números ingresados es: ", contador)
print("El promedio es: ", promedio)

print("PROCESO TERMINADO!")
