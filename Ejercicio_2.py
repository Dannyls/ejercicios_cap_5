#Programa que pida una lista de números y al final
#muestre por pantalla el máximo y mínimo de los números.
# ___Author___ = "Danny Lima"
# ___Email___ = "danny.lima@unl.edu.ec

lista=[]
contador=0
suma=0

while True:
    numero=input("Ingrese un número: ")
    lista.append(numero)
    if numero.lower() in "fin":
        break
    try:
        suma = suma + int(numero)
        contador = contador + 1
        máximo= max(lista)
        mínimo= min(lista)

    except ValueError:
        print("Entrada inválida")

print("La suma es: ", suma)
print("La total de números ingresados es: ", contador)
print("El número menor es: ", mínimo)
print("El número mayor es: ", máximo)

print("PROCESO TERMINADO!")
